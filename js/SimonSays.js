const colourArray = {0:"red", 1:"blue", 2:"yellow", 3:"green"}
const baseNum = 3;

let currentLevel = 1;
let pattern = [];

function createPattern (num) {
    num = parseInt(num) + baseNum - 1;
    pattern = [];
    $('#simon').empty();

    for(let i = 0; i < parseInt(num); i++) {
        let colour = colourArray[Math.floor(Math.random() * 4)];
        pattern.push(colour);
        $('#simon').append(`<div class="pattern" id=simonColour${i} style="background-color: ${colour}"> </div>`);
    }
    $('#simon').css('visibility', 'visible');
    hidePattern();
}

// Creates the pattern to be followed.
// param:  level is the current level that the player is on.
function createLevel (level) {
    createPattern(level);


}

function hidePattern() {
    //$("#simon").delay(3000).hide(500);
    $('#simon')
        .delay(3000)
        .queue(function (next) {
            $(this).css('visibility', 'hidden');
            next();
            addGameBoard(1);
        });
    
}

function addGameBoard () {

    let inputNumber = 0;

    

    for (let i = 0; i < 4; i++) {
        $("#gameboard").append(`<div class="pattern gametile" id="gameColour${i}" style="background-color: ${colourArray[i]}"> </div>`);
        Randomize();
        $(`#gameColour${i}`).click(function (item) {
            let gameover = false;
            if (pattern[inputNumber++] == item.target.style.backgroundColor) {
                console.log("correct");
            }
            else {
                gameover = true;
                alert("Incorrect");
                $("#gameboard").empty();
                $("#gameboard").append('<input type="button" onclick="StartOver()" value="Try Again" />');
            }
            if (inputNumber == pattern.length && !gameover ) {
                $("#gameboard").empty();
                $("#gameboard").append('<input type="button" onclick="IncreaseLevel()" value="Next level" />');
            }
        })
    }

    $("#gameColour1").delay((currentLevel + 2 ) * 1000)
        .queue(function() {
            TimedOut()
        });
    
}

function IncreaseLevel () {
    $("#gameboard").empty();
    currentLevel = currentLevel + 1;
    createLevel(currentLevel);
}

function TimedOut() {
    alert("out of time");
    $("#gameboard").empty();
    $("#gameboard").append('<input type="button" onclick="StartOver()" value="Start Over" />');
}

function StartOver () {
    $("#gameboard").empty();
    currentLevel = 1;
    createLevel(currentLevel);
}

function Randomize() {
    var ul = document.querySelector('#gameboard');
    for (var i = ul.children.length; i >= 0; i--) {
        ul.appendChild(ul.children[Math.random() * i | 0]);
    }
}